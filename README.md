* Ritter

`ritter` is a web-development acceptance-testing tool, utilising
screenshots and visual-diffs. (Similar to the BBC's `wraith`)
