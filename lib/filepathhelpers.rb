class FilePathHelpers
  def self.sanitise(string)
    string.gsub(/[^\w\.]/, '_')
  end

  def self.tweak(string)
    # This is an ad-hoc customisation, for when we capture a root path
    # ("/"), then we'd like it to stand out more, in the filename, and
    # still sort to the top.
    #
    # Specifically, a single forward-slash, will be turned into
    # "__index"
    #
    string.gsub(/^\/$/, '__index')
  end

  def self.relative_from(full_filepath, relative_root)
    full = Pathname.new(full_filepath)
    root = Pathname.new(relative_root)
    return full.relative_path_from(root).to_s
  end

  def self.relative_to_output(full_filepath)
    return relative_from(full_filepath, File.expand_path("..", OUTPUT_DIR))
  end

  def self.output_filename(*params)
    File.join(OUTPUT_DIR, "#{parameter_filename(*params)}.jpg")
  end

  def self.parameter_filename(*params)
    params.map(&:to_s).map(&self.method(:tweak)).map(&self.method(:sanitise)).join("-")
  end

  def self.difference_filename(file1, file2)
    # [NOTE:] This makes certain assumptions about how the filename is
    # generated, in the original/main loop.
    #
    base_filename = File.basename(file1, File.extname(file1))
    timestamp, target_name, uri = base_filename.split("-")

    output_filename timestamp, '__diff', uri
  end
end
