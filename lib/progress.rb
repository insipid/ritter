require 'yaml'

class Progress
  # [FIXME:] Obviously this is stupid.
  @@progress_shots = Hash.new(nil)
  @@progress_diffs = Hash.new(nil)
  @@progress_montages = Hash.new(nil)

  def self.register_screenshot(state)
    # Better way to remove the prefix string (although still non-intuitive)
    path = state[:uri].to_s.slice(state[:target][:host].length .. -1)

    @@progress_shots[path] = (@@progress_shots[path] || []).push state

    if @@progress_shots[path].length == 2
      filenames = @@progress_shots[path].map { |s| s[:filename] }
      NormaliseImageTask.new(state.merge({ filenames: filenames }))
    else
      # puts "DEBUG: Shot #{path}, total: #{@@progress_shots[path].length}"
    end
  end

  def self.register_comparison(state)
    # Better way to remove the prefix string (although still non-intuitive)
    path = state[:uri].to_s.slice(state[:target][:host].length .. -1)

    @@progress_diffs[path] = { path: path, file: state[:comparison][:file], difference: state[:comparison][:difference] }
  end

  def self.register_montage(state)
    # Better way to remove the prefix string (although still non-intuitive)
    path = state[:uri].to_s.slice(state[:target][:host].length .. -1)

    @@progress_montages[path] = { path: path, montage: state[:montage_file] }
  end

  def self.show_diffs()
    # [FIXME:] Want to actually sort by *number of slashes* first, *then* length, *then* alpha
    @@progress_diffs.keys.sort { |a, b| (a.length <=> b.length) && (a <=> b) }.map do |key|
      d = @@progress_diffs[key]
      puts "#{d[:path]} -- #{d[:file]} -- #{d[:difference]}"
    end
  end

  def self.show_montages()
    # [FIXME:] Want to actually sort by *number of slashes* first, *then* length, *then* alpha
    @@progress_montages.keys.sort { |a, b| (a.length <=> b.length) && (a <=> b) }.map do |key|
      d = @@progress_montages[key]
      puts "#{d[:path]} -- #{d[:montage_file]}"
    end
  end
end
