module Spider
  class Simple
    def initialize(options = Hash.new(nil))
      hydra_options = { max_concurrency: 2 }.merge(options[:hydra] || {})
      @request_options = { followlocation: true, timeout: 60 }.merge(options[:request] || {})
      @max_paths = options[:max_paths] || 20
      @debug = !!options[:debug]

      @hydra = Typhoeus::Hydra.new(hydra_options)
      @seen = []
    end

    def on_complete(response)
      # puts "COMPLETED: #{response.effective_url}"
      extract_href_uris(response.body).each do |uri|
        # Only add new URIs, and only if we're not full
        if !@seen.include?(uri) && (@seen.length < @max_paths) then
          puts "ADDING: #{uri}" if @debug
          @seen << uri
          @hydra.queue request(uri)
        else
          # puts "IGNORING: #{uri} (Seen: #{@seen.include?(new_u)})/#{@seen.length})"
        end
      end
    end

    def request(url)
      request = Typhoeus::Request.new(url, @request_options)
      request.on_complete(&self.method(:on_complete))
      request
    end

    def crawl(base_url)
      @hydra.queue request(base_url)
      # This starts the queue running (which we keep populating from
      # on_complete calls; it blocks until the queue is emptied.
      @hydra.run

      @seen
    end

    def extract_href_uris(body)
      Nokogiri::HTML(body).xpath('//a[@href]').map { |e| e.attr('href') }.map do |h|
        begin
          u = URI(h)

          if (!u.host.nil? && (u.host != BASE_URI.host)) || !['http', 'https', nil].include?(u.scheme) then
            nil
          else
            u.scheme = BASE_URI.scheme if u.scheme.nil?
            u.host = BASE_URI.host if u.host.nil?

            # Postel's Law? :(
            u.path = "/#{u.path}" if u.path[0] != '/'

            # Strip off fragment (if any)
            URI(u.to_s.match(/^([^#]*)#?.*$/).captures.first)
          end

        rescue URI::InvalidURIError
          puts "ERROR: Invalid URI: #{h}"
          nil
        end
      end.compact.uniq
    end
  end
end
