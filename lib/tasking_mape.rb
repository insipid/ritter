# Module for task- and queue-related base classes. The Queue class
# organises and dispatches; the GenericTask is a(n abstract) class
# that actually does the work.

module TaskingMape

  # The "tasking" class just creates a thread-pool to work through the
  # queue. Should be a decent start, since most of our work will be done
  # by child processes.
  #
  class Queue
    def initialize(options = Hash.new(nil))
      @max_threads = options[:size] || 2
      @q = ::Queue.new
    end

    def push(object)
      @q.enq object
      self
    end

    def prepare_threads
      @threads = @max_threads.times.map do
        Thread.new do
          while (object = @q.deq) != :stop
            object.execute
          end
        end
      end
    end

    def pump
      # We need to re-prepare the threads every time we start pumping
      # (not just the first time)
      prepare_threads

      # Fill the queue with sentinel values; one gets consumed by each
      # thread when it finishes.
      @threads.each do
        @q.enq :stop
      end

      # "Join" on all threads, which means we block from here, until all
      # threads terminate.
      @threads.each(&:join)

      # I don't care, rubocop; I *like* explicit returns :)
      return @q.length
    end

    def pump_repeatedly
      while (leftover = self.pump) > 0
        # puts "DEBUG: Original queue exhausted, restarting with #{leftover} elements"
      end
    end
  end

  class GenericTask
    @@task_queue = nil

    attr_accessor :state

    def initialize(state)
      @state = state
      @@task_queue ||= TaskingMape::Queue.new
      @@task_queue.push self
    end

    def execute
      # Put your actual task work here
      raise "Can't execute a GenericTask -- subclass it, and execute that"
    end

    def self.start_all
      @@task_queue.pump
    end

    def self.exhaust_all
      @@task_queue.pump_repeatedly
    end
  end
end
