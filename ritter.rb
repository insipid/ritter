require 'typhoeus'
require 'nokogiri'
require './lib/spider'
require './lib/tasking_mape'
require './lib/progress'
require './lib/filepathhelpers'
require 'posix/spawn'
require 'pathname'

# Wow! So, the project is named 'ritter', at least in part because of
# the legendary John Ritter; and while checking out something in the
# Nokogiri docs, I find this in one of their examples:
#
#   doc = Nokogiri::HTML(open("http://www.threescompany.com/"))
#
# :D :D :D
#

BASE_URI = URI("http://test.digital.library.lse.ac.uk/")
OUTPUT_DIR = File.expand_path("./output")
SCREENSHOT_TIMEOUT = 30 # seconds

FileUtils.mkdir_p OUTPUT_DIR

uris = Spider::Simple.new({ debug: false, max_paths: 20 }).crawl(BASE_URI).map { |u| u.query.nil? ? u.path : "#{u.path}?#{u.query}" }
# uris = %w{ /
#  /browse
#  /collections/streetlifeinlondon
#  /exhibitions
# }

#TARGETS = [{ name: 'test', host: "http://test.digital.library.lse.ac.uk" },
TARGETS = [{ name: 'test', host: "http://158.143.197.183:3000" },
           { name: 'live', host: "http://digital.library.lse.ac.uk" }]


class ScreenshotTask < TaskingMape::GenericTask
  def execute
    child = POSIX::Spawn::Child.build('phantomjs', 'screenshot.js', state[:uri].to_s, state[:filename], timeout: SCREENSHOT_TIMEOUT)

    begin
      relative_filename = FilePathHelpers.relative_to_output(state[:filename])
      puts "Screenshot task: Capturing #{@state[:uri]} to #{relative_filename}"
      child.exec!
      Progress.register_screenshot state

    rescue POSIX::Spawn::TimeoutExceeded
      puts "ERROR: Timeout exceeded, shooting #{state[:uri]}"
      # [TODO:] Progress.register_failure ?
    end

    puts "STDOUT: #{child.out}" unless child.out.empty?
    puts "STDERR: #{child.err}" unless child.err.empty?
  end
end

class ResizeImageTask < TaskingMape::GenericTask
  def execute
    file1, file2 = state[:filenames][0..1]

    output_filename1 = file1.gsub(/\.jpg$/, '.resized.jpg')
    child = POSIX::Spawn::Child.new('convert', file1, '-extent', "#{state[:width]}x#{state[:height]}", '-gravity', 'NorthWest', '-background', 'none', output_filename1)

    output_filename2 = file2.gsub(/\.jpg$/, '.resized.jpg')
    child = POSIX::Spawn::Child.new('convert', file2, '-extent', "#{state[:width]}x#{state[:height]}", '-gravity', 'NorthWest', '-background', 'none', output_filename2)

    ComparisonTask.new(state.merge({ filenames: [output_filename1, output_filename2] }))
  end
end

class NormaliseImageTask < TaskingMape::GenericTask
  def execute
    file1, file2 = state[:filenames][0..1]
    # [FIXME:] Error-handling
    child = POSIX::Spawn::Child.new('identify', file1, file2)

    # Yes this regex is a bit nasty -- we're processing the output of
    # the "identify" command. Arguably, we could just use RMagick, but
    # that wouldn't replace ImageMagick /everywhere else/, so might as
    # well just get used to the CLI stuff here.
    #
    re = /(?<filename>[^\[ ]+).* (?<width>[0-9]+)x(?<height>[0-9]+)/

    max_width = 0
    max_height = 0

    child.out.split(/\r?\n/).each do |t|
      md = re.match(t)
      max_width = md[:width].to_i if md[:width].to_i > max_width
      max_height = md[:height].to_i if md[:height].to_i > max_height
    end

    ResizeImageTask.new(state.merge({ width: max_width, height: max_height, filenames: [file1, file2] }))

    # puts "Maxes: #{max_width} x #{max_height}"
    # Progress.register_comparison state.merge({ comparison: { file: output_file, difference: fuzzed_difference }})
  end
end

class MontageTask < TaskingMape::GenericTask
  def execute
    file1, file2 = state[:filenames][0..1].sort
    output_file = state[:difference_file].gsub(/__diff/, '__montage')

    child = POSIX::Spawn::Child.new('montage', '-tile', '3x1', '-geometry', '+0+0', file1, file2, state[:difference_file], output_file)

    puts "STDOUT: #{child.out}" unless child.out.empty?
    puts "STDERR: #{child.err}" unless child.err.empty?

    Progress.register_montage state.merge({ montage_file: output_file })
  end
end

class ComparisonTask < TaskingMape::GenericTask
  def execute
    # [FIXME:] The ".sort" makes sure the files are always compared in
    # the *same* order (and thus 'left' and 'right' are consistent for
    # the comparisons), but what we *really* want is for it to be
    # user-specified ordering, as well. (e.g., I always want 'live' on
    # the left).
    #
    file1, file2 = state[:filenames][0..1].sort
    output_file = FilePathHelpers.difference_filename(file1, file2)

    child = POSIX::Spawn::Child.new('compare', '-metric', 'FUZZ', file1, file2, output_file)
    relative_filenames_s = [file1, file2].map(&FilePathHelpers.method(:relative_to_output)).join(', ')

    #puts "STDOUT: #{child.out}" unless child.out.empty?
    #puts "STDERR: #{child.err}" unless child.err.empty?

    fuzzed_difference = /[^0-9\.]*([0-9\.]+)/.match(child.err) { |md| md.captures.first }

    puts "Comparing: #{relative_filenames_s} into #{output_file} -- DIFF: #{fuzzed_difference}"

    MontageTask.new(state.merge({ filenames: [file1, file2], difference_file: output_file }))

    #Progress.register_comparison state.merge({ comparison: { file: output_file, difference: fuzzed_difference }})
  end
end

uris.each do |uri|
  TARGETS.each do |target|
    request_uri = URI.join(target[:host], uri)
    output_filename = FilePathHelpers.output_filename Time.now.to_i, target[:name], uri
    ScreenshotTask.new({ uri: request_uri, filename: output_filename, target: target })
  end
end

ScreenshotTask.exhaust_all

puts "Queue exhausted"

Progress.show_montages
