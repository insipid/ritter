var system = require('system');
var page = require('webpage').create();

var url = system.args[1];
var filename = system.args[2];

// page.viewportSize = { width: view_port_width, height: 1500};
// page.viewportSize = { width: 1024, height: 1600 };
page.viewportSize = { width: 1024 };
page.settings = { loadImages: true, javascriptEnabled: true };

// If you want to use additional phantomjs commands, place them here
page.settings.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.17';

page.open(url, function(status) {
  var background = page.evaluate(function() {
    return document.body.style.background;
  });

  page.evaluate(function() {
    document.body.bgColor = "white";
  });

  var background = page.evaluate(function() {
    $('body').css('background', '');
  });

  var title = page.evaluate(function() {
    return document.title;
  });

  // console.log('Title: ' + title);

  page.render(filename);
  phantom.exit();
});
